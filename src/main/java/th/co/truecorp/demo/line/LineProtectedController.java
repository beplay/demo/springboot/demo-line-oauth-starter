package th.co.truecorp.demo.line;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.ApplicationArguments;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import th.co.truecorp.springboot.line.LineException;
import th.co.truecorp.springboot.line.LineProfileAPI;
import th.co.truecorp.springboot.line.model.LineUserProfile;

@RestController
@RequestMapping("/line")
public class LineProtectedController {

	/**
	 * Sample protected method 1
	 */
	@GetMapping(path="/register")
	public String register() {
		return "Register";
	}
	
	@GetMapping(path="/public/query")
	public  String publicQuery() {
		return "Public query";
	}
	
	@GetMapping(path="/query")
	public String protectedQuery(){
		return "Protected query";
	}
	
	@Autowired 
	private LineProfileAPI api;
	
	@GetMapping(
			path="/profile"
			, produces="application/json; charset=UTF-8"
		)
	public LineUserProfile getProfile() throws LineException {
		// Get User profile 
		return api.getUserProfile();
	}
}
