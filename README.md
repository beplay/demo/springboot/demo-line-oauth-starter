# LINE starter

## Usage

### Add repository & dependencies

```groovy
repositories {
  mavenCentral()
  maven {
    url "http://repository.devops.arctic.true.th/repository/maven-public/"
  }
}

dependencies {
  compile 'th.co.truecorp.springboot:line-oauth-starter:1.0.+'
}
```

### Usage

#### Configuration to protected resources

##### application.yml

```yml
line:
  security:
    protectedPaths
      - /xxx/yyy
      - /xxx/zzz
      - /line/profile
```

##### application.properties

```properties
line.security.protectedPaths[0]=/xxx/yyy
line.security.protectedPaths[1]=/xxx/zzz
line.security.protectedPaths[2]=/line/profile
```

#### Get LINE user profile

Add `@Autowired LineUserProfile` to Java code

#### example

Server side path must be defined in configuration

```java
@RestController
@RequestMapping("/line")
public class LineProtectedController {

  @GetMapping(
      path="/profile"
      , produces="application/json; charset=UTF-8"
    )
  public LineUserProfile getProfile() throws LineException {

    // Get User profile
    return api.getUserProfile();
  }
}
```

#### Testing invoke protected API with accessToken

```sh
curl \
-H "Authorization: Bearer <acccessToken>" \
-i "http://localhost:8080/line/profile"
```

#### Sample result

```json
{
  "userId": "U9732b1b50c07153497373af08cd45329",
  "displayName": "ปูแมน",
  "pictureUrl": "https://profile.line-scdn.net/6fc4cb3a3ed5"
}
```